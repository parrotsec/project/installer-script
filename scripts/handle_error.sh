#!/bin/bash

handle_error() {
    echo "Error: $1" >&2
    exit 1
}
